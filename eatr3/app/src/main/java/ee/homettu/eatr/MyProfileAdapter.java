package ee.homettu.eatr;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

/**
 * Created by odin on 18.03.15.
 */
public class MyProfileAdapter<P> extends ParseQueryAdapter {

    public MyProfileAdapter(Context context, String className) {
        super(context, className);
    }

    public MyProfileAdapter(Context context, QueryFactory<ParseObject> queryFactory) {
        super(context, queryFactory);
    }

    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.custom_gridview, null);
        }
        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(object, v, parent);


        ParseImageView descriptionView = (ParseImageView) v.findViewById( android.R.id.icon);
        descriptionView.setParseFile(object.getParseFile("image"));
        descriptionView.loadInBackground();

        return v;

    }

}
