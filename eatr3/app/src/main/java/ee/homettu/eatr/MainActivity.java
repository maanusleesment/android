package ee.homettu.eatr;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;

public class MainActivity extends Activity {

    private EditText  username=null;
    private EditText  password=null;
    private TextView attempts;
    private Button login;
    int counter = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        //attempts = (TextView)findViewById(R.id.password);
        //attempts.setText(Integer.toString(counter));
        login = (Button)findViewById(R.id.email_sign_in_button);
    }

    public void login(View view){
        try {

            ParseUser.logIn(username.getText().toString(), password.getText().toString());
            String lastView = ParseUser.getCurrentUser().getEmail();
            if(null==lastView) {//esimene kasutaja
                startActivity(new Intent(this, FindMatchesActivity.class));
                finish();
            } else {
                switch (lastView) {
                    case "" + MyProfile.VIEW_CONSTANT:
                        startActivity(new Intent(this, MyProfile.class));
                        break;
                    case "" + FindMatchesActivity.VIEW_CONSTANT:
                        startActivity(new Intent(this, FindMatchesActivity.class));
                        break;
                    case "" + UserLikesActivity.VIEW_CONSTANT:
                        startActivity(new Intent(this, UserLikesActivity.class));
                        break;
                }
                finish();
            }
        } catch (ParseException e) {
            Toast.makeText(getApplicationContext(), "Wrong Credentials",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();

        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "Something went wrong",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();

        }
    }

}