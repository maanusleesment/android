package ee.homettu.eatr;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.Arrays;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class FindMatchesActivity extends Activity {

    @InjectView(R.id.frame) SwipeFlingAdapterView flingContainer;

    public static final int VIEW_CONSTANT = 3;
    FindMatchesAdapter adapter;
    private ParseObject lastCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_matches);
        ButterKnife.inject(this);

        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.setEmail(this.VIEW_CONSTANT+""); // hoiab meeles viimase vaate
        currentUser.saveInBackground();

        adapter = new FindMatchesAdapter(getApplicationContext(), R.layout.tinder_like_card, R.id.cardImage);
        flingContainer.setAdapter(adapter);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("FoodImage");
        query.whereNotEqualTo("user", ParseUser.getCurrentUser());
        query.whereNotEqualTo("user_likes", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                //if e null
                if(e == null) {
                    adapter.addAll(parseObjects);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                lastCard = adapter.getItem(0);
                adapter.remove(adapter.getItem(0));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                ParseObject parseObject = new ParseObject("FoodImage");
                parseObject.put("user_likes", adapter.getItem(0).getObjectId());
                parseObject.put("like", "dislike");
                parseObject.setACL(new ParseACL(ParseUser.getCurrentUser()));
                parseObject.saveInBackground();
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                // Retrieve the object by id
                ParseQuery<ParseObject> query = ParseQuery.getQuery("FoodImage");
                query.getInBackground(lastCard.getObjectId(), new GetCallback<ParseObject>() {
                    public void done(ParseObject userLikes, ParseException e) {
                        if (e == null) {

                            userLikes.add("user_likes", ParseUser.getCurrentUser().getObjectId());
                            userLikes.saveInBackground();
                        }
                    }
                });
                showDialog( ((ParseObject)dataObject).getObjectId() );
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here
                //mCards.add(null);
                /*adapter.remove(adapter.getItem(0));

                Log.d("LIST", "notified");*/

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                makeToast(FindMatchesActivity.this, ""+dataObject);
            }
        });

    }

    static void makeToast(Context ctx, String s){
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.right)
    public void right() {
        /**
         * Trigger the right event manually.
         */
        if(!adapter.isEmpty())
            flingContainer.getTopCardListener().selectRight();
        else
            makeToast(FindMatchesActivity.this, "No more meals to display");
    }

    @OnClick(R.id.left)
    public void left() {
        if(!adapter.isEmpty())
            flingContainer.getTopCardListener().selectLeft();
    }

    private void showDialog(String id) {
        // custom dialog
        final String objectId = id;
        final Dialog dialog = new Dialog(FindMatchesActivity.this);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("You have liked this image");

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Description: " + lastCard.get("description").toString());

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDetailView = new Intent(FindMatchesActivity.this, ItemDetail.class);
                goToDetailView.putExtra("objectId", objectId);
                goToDetailView.putExtra("parentView", FindMatchesActivity.VIEW_CONSTANT);
                startActivity(goToDetailView);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_user).setTitle(ParseUser.getCurrentUser().getUsername());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_feed:
                startActivity(new Intent(this, FindMatchesActivity.class));
                finish();
                return true;

            case R.id.menu_settings:
                startActivity(new Intent(this, MyProfile.class));
                finish();
                return true;

            case R.id.menu_user_likes:
                startActivity(new Intent(this, UserLikesActivity.class));
                finish();
                return true;

            case R.id.menu_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}



