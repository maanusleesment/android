package ee.homettu.eatr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.ParseImageView;
import com.parse.ParseObject;

import java.util.List;


/**
 * Created by odin on 16.03.15.
 */
public class FindMatchesAdapter extends ArrayAdapter<ParseObject>{
    private final Context mContext;
    private final int mResource;
    private final int mImageViewId;
    private final LayoutInflater mInflater;

    public FindMatchesAdapter(Context context, int resource, int ImageViewId) {
        super(context, resource, ImageViewId);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mImageViewId = ImageViewId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.tinder_like_card, parent, false);
        } else {
            view = convertView;
        }

        ParseImageView imageViewPhoto = (ParseImageView) view.findViewById(R.id.cardImage);
        ParseObject photo = getItem(position);
        imageViewPhoto.setParseFile(photo.getParseFile("image"));
        imageViewPhoto.loadInBackground();

        return view;
    }

}
