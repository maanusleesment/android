package ee.homettu.eatr;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

/**
 * Created by odin on 18.03.15.
 */
public class CustomParseQueryAdapter<P> extends ParseQueryAdapter {

    public CustomParseQueryAdapter(Context context, QueryFactory<ParseObject> className) {
        super(context, className);
    }

    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.custom_listview, null);
        }
        // Take advantage of ParseQueryAdapter's getItemView logic for
        // populating the main TextView/ImageView.
        // The IDs in your custom layout must match what ParseQueryAdapter expects
        // if it will be populating a TextView or ImageView for you.
        super.getItemView(object, v, parent);


        ParseImageView descriptionView = (ParseImageView) v.findViewById( android.R.id.icon);
        descriptionView.setParseFile(object.getParseFile("image"));
        descriptionView.loadInBackground();

        TextView textView = (TextView) v.findViewById(android.R.id.text1);
        textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        textView.setText(object.get("description").toString());
        return v;

    }

}
