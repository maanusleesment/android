package ee.homettu.eatr;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class UserLikesActivity extends ListActivity {


    public static final int VIEW_CONSTANT = 2;

    //CustomParseQueryAdapter<ParseObject> adapter = new CustomParseQueryAdapter<ParseObject>(this, "FoodImage");
    CustomParseQueryAdapter<ParseObject> adapter =
            new CustomParseQueryAdapter<ParseObject>(this, new CustomParseQueryAdapter.QueryFactory<ParseObject>() {
                public ParseQuery<ParseObject> create() {
                    ParseQuery query = new ParseQuery("FoodImage");
                    Log.d("UserLikesActivity", ParseUser.getCurrentUser().getObjectId());
                    query.whereEqualTo("user_likes", ParseUser.getCurrentUser().getObjectId());
                    return query;
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_likes);

        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.setEmail(this.VIEW_CONSTANT+""); // hoiab meeles viimase vaate
        currentUser.saveInBackground();


        adapter.setTextKey("user"); // mingi "liked on <date>" äkki
        adapter.setImageKey("image");

        ListView listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        ParseObject listItemObject = adapter.getItem(position);
        String objectId = listItemObject.getObjectId();

        Intent goToDetailView = new Intent(UserLikesActivity.this, ItemDetail.class);
        goToDetailView.putExtra("objectId", objectId);
        goToDetailView.putExtra("parentView", UserLikesActivity.VIEW_CONSTANT);
        startActivity(goToDetailView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_user).setTitle(ParseUser.getCurrentUser().getUsername());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_feed:
                startActivity(new Intent(this, FindMatchesActivity.class));
                return true;

            case R.id.menu_settings:
                startActivity(new Intent(this, MyProfile.class));
                return true;

            case R.id.menu_user_likes:
                startActivity(new Intent(this, UserLikesActivity.class));
                return true;

            case R.id.menu_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
