package ee.homettu.eatr;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;


public class MyProfile extends Activity {

    public static final int VIEW_CONSTANT = 1;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE    = 2;
    private MyProfileAdapter<ParseObject> adapter;
    private boolean galleryOptionButtonEnabled = false;
    private boolean  cameraOptionButtonEnabled = false;
    private String imageDescription;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if(requestCode == REQUEST_CAMERA) {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/temp.png");
                Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
                uploadImageWithData(image);

                clearBitmap(image);
                file.delete();
            } else if (requestCode == SELECT_FILE) {
                Uri selectImageUri = data.getData();
                String tempImagePath = getRealPathFromUri(selectImageUri);
                Bitmap image = BitmapFactory.decodeFile(tempImagePath);
                uploadImageWithData(image);

                clearBitmap(image);
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void uploadImageWithData(final Bitmap image) {
        ParseObject parseObject = new ParseObject("FoodImage");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 60, stream); //null, kui galleryga
        byte[] bitmapdata = stream.toByteArray();

        ParseFile imgfile = new ParseFile("photo.jpg", bitmapdata);
        parseObject.put("image", imgfile);
        parseObject.put("user", ParseUser.getCurrentUser());
        parseObject.put("user_likes", Arrays.asList());
        parseObject.put("description", imageDescription);
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Toast.makeText(MyProfile.this, "Photo uploaded!", Toast.LENGTH_LONG).show();
                clearBitmap(image);
                startActivity(new Intent(MyProfile.this, MyProfile.class));
                finish();
            }
        });
    }

    private String getRealPathFromUri(Uri contentUri) {
        String res = null;
        String [] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor  = getContentResolver().query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public static void clearBitmap(Bitmap bm) {
        bm.recycle();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.setEmail(this.VIEW_CONSTANT+""); // hoiab meeles viimase vaate
        currentUser.saveInBackground();


        adapter = new MyProfileAdapter<ParseObject>(this, new ParseQueryAdapter.QueryFactory<ParseObject>() {
                    public ParseQuery<ParseObject> create() {
                        ParseQuery query = new ParseQuery("FoodImage");
                        query.whereEqualTo("user", ParseUser.getCurrentUser());
                        return query;
                    }
                });
        adapter.setImageKey("image");

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent goToDetailView = new Intent(MyProfile.this, ItemDetail.class);
                goToDetailView.putExtra("objectId", adapter.getItem(position).getObjectId());
                goToDetailView.putExtra("parentView", MyProfile.VIEW_CONSTANT);
                startActivity(goToDetailView);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_user).setTitle(ParseUser.getCurrentUser().getUsername());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_feed:
                startActivity(new Intent(this, FindMatchesActivity.class));
                finish();
                return true;

            case R.id.menu_settings:
                startActivity(new Intent(this, MyProfile.class));
                finish();
                return true;

            case R.id.menu_user_likes:
                startActivity(new Intent(this, UserLikesActivity.class));
                finish();
                return true;

            case R.id.menu_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    * Custom Dialogs*/
    public void showRenameUsernameDialog(View v) {
        final Dialog dialog = new Dialog(MyProfile.this);
        dialog.setContentView(R.layout.dialog_rename_user);
        dialog.setTitle("Rename username");

        Button dialogButton = (Button) dialog.findViewById(R.id.submitPicture);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            EditText editText = (EditText) dialog.findViewById(R.id.newUsernameField);
            final String newUserName = editText.getText().toString().trim();
            if(!newUserName.matches("^[a-z0-9_-]{3,16}$")) {
                Toast.makeText(MyProfile.this, "Not a valid username" , Toast.LENGTH_SHORT).show();
                return;
            }
            ParseUser currentUser = ParseUser.getCurrentUser();
            currentUser.setUsername(newUserName);
            currentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    startActivity(new Intent(MyProfile.this, MyProfile.class));
                    dialog.dismiss();
                    finish();
                }
            });
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showAddPictureDialog(View v) {
        final Dialog dialog = new Dialog(MyProfile.this);
        dialog.setContentView(R.layout.dialog_add_picture);
        dialog.setTitle("Add picture with:");

        final Button dialogButton = (Button) dialog.findViewById(R.id.submitPicture);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText mEdit   = (EditText) dialog.findViewById(R.id.addImageEditText);
                imageDescription = mEdit.getText().toString();
                Log.v("EditText", imageDescription);
                if(cameraOptionButtonEnabled) {
                    Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "temp.png");
                    takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(takePhotoIntent, REQUEST_CAMERA); //j22b vastust ootama "forResult"
                } else if(galleryOptionButtonEnabled) {
                    Intent choosePhotoIntent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    choosePhotoIntent.setType("image/*"); //suvaline
                    startActivityForResult(Intent.createChooser(choosePhotoIntent, "Select File"), SELECT_FILE);
                }
            }
        });

        final CheckBox cameraOptionButton = (CheckBox) dialog.findViewById(R.id.camera_radio_button);
        cameraOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraOptionButtonEnabled = !cameraOptionButtonEnabled;

                //only one is picked
                dialogButton.setEnabled(cameraOptionButtonEnabled != galleryOptionButtonEnabled);

                cameraOptionButton.setChecked(cameraOptionButtonEnabled);
            }
        });

        final CheckBox galleryOptionButton = (CheckBox) dialog.findViewById(R.id.gallery_radio_button);
        galleryOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryOptionButtonEnabled = !galleryOptionButtonEnabled;

                //only one is picked
                dialogButton.setEnabled(cameraOptionButtonEnabled != galleryOptionButtonEnabled);

                galleryOptionButton.setChecked(galleryOptionButtonEnabled);
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            dialog.dismiss();
            }
        });

        dialog.show();

    }

 }
