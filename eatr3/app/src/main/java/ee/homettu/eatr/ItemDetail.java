package ee.homettu.eatr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;

import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Arrays;


public class ItemDetail extends Activity {

    private String parseObjectId;
    private int parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        parseObjectId = getIntent().getStringExtra("objectId");
        parentView = getIntent().getIntExtra("parentView", -1); // default -1

        if (parentView == FindMatchesActivity.VIEW_CONSTANT) {
            Button button = (Button) findViewById(R.id.remove_button);
            button.setVisibility(View.GONE);
        } else if(parentView != UserLikesActivity.VIEW_CONSTANT) {
            Button btn = (Button) findViewById(R.id.fbButton);
            btn.setVisibility(View.GONE);
        }
        getData();
    }
    public void getData() {
        ParseQuery.getQuery("FoodImage").getInBackground(parseObjectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e == null) {
                    setTitle("Meal details");
                    ParseImageView imageViewPhoto = (ParseImageView) findViewById(R.id.image);
                    imageViewPhoto.setParseFile(parseObject.getParseFile("image"));
                    imageViewPhoto.loadInBackground();
                }
                }
        });

    }

    public void removeFromDb(View v) {
        ParseQuery.getQuery("FoodImage").getInBackground(parseObjectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                switch (parentView) {
                    case MyProfile.VIEW_CONSTANT:
                        removeUserImage(parseObject);
                        break;
                    case UserLikesActivity.VIEW_CONSTANT:
                        removeUserLike(parseObject);
                        break;
                }
            }
        });
    }

    public void shareToFacebook (View view) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Wow! This meal looks absolutely amazing!");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share with your friends"));
    }

    private void removeUserImage(ParseObject parseObject) {
        parseObject.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                //go back to MyProfile "Settings"
                startActivity(new Intent(ItemDetail.this, MyProfile.class));
            }
        });
    }

    private void removeUserLike(ParseObject parseObject) {
        parseObject.removeAll("user_likes", Arrays.asList(ParseUser.getCurrentUser().getObjectId()));
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                //go back to MyProfile "Settings"
                startActivity(new Intent(ItemDetail.this, UserLikesActivity.class));
            }
        });
    }

}
